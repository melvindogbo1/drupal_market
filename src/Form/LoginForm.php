<?php

namespace Drupal\auth\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * LoginForm controller.
 */
class LoginForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   *   ().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'auth_login';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Authentification'),
    ];

    $form["username"] = [
      '#type' => 'textfield',
      '#attributes' => array(
        'placeholder' => t('Username'),
      ),
      
      '#required' => TRUE,
    ];

    $form["password"] = [
      "#type" => "password",
      '#attributes' => array(
        'placeholder' => t('Password'),
    ),
      "#required" => TRUE
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Se connecter'),
      '#attributes' => array(
       'class' => array('btn','btn-primary','w-100','justify-content-center'),
    ),
    ];

    // $form = parent::buildForm($form, $form_state);

    return $form;

  }

  /**
   * Validate the title and the checkbox of the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $username = $form_state->getValue('username');
    $password = $form_state->getValue('password');
    if(empty(trim($username))){
      $form_state->setErrorByName('username', $this->t("Le nom d'utilisateur ne peut pas être vide"));
    }
    if(empty(trim($password))){
      $form_state->setErrorByName('password', $this->t("Le mot de passe ne peut pas être vide"));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    // call external server
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"http://192.168.31.78:8080/ci.orange.ldap/rs-interface/authenticate");
    // curl_setopt($ch, CURLOPT_URL,"http://demo0344786.mockable.io/test3");
    curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,
                // 'username='.$form_state->getValue('username').'&password='.$form_state->getValue('password').'\'');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
        'username' => $form_state->getValue('username'),
        'password' => $form_state->getValue('password')
      )
    ));
    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);

    if(curl_exec($ch) === false)
    {
        $messenger->addMessage('Erreur Curl : ' . curl_error($ch));
    }
    else
    {
      // convert xml to json
      $xml = simplexml_load_string($server_output);
      $json = json_encode($xml);
      $user = json_decode($json,TRUE);
      $messenger->addMessage($json);
      if($user["check"] == "true"){
        $ids = \Drupal::entityQuery('user')
                ->condition('mail', $user['email'])
                ->execute();
        // Détermination de la fonction de l'utilisateur (directeur, manager, cadre ou stagiaire) 
        switch (true) {
          case (strpos($user['fonction'], 'Directeur') !== false):
            $user['fonction'] = 'Directeur';
            break;

          case (strpos($user['fonction'], 'Manager') !== false):
            $user['fonction'] = 'Manager';
            break;
      
          case (strpos($user['fonction'], 'Stagiaire') !== false):
            $user['fonction'] = 'Stagiaire';
            break;

          default:
            $user['fonction'] = 'Cadre';
            break;
       }
        if (!empty($ids)) {
          foreach($ids as $key => $value){
            $id = (int) $value;
            break;
          }
          $existingUser = User::load($id);
          $existingUser->save();
          user_login_finalize($existingUser);
          $messenger->addMessage($existingUser->getEmail().' find');
        }else {
          $newUser = User::create();
          $newUser->setPassword("password");
          $newUser->enforceIsNew();
          $newUser->setEmail($user['email']);

          // Attribution du username en se basant sur le matricule saisi 
          $newUser->setUsername($form_state->getValue('username'));
          $newUser->activate();

          // Direction dédiée 
          $direction_vocabulary = $this->getVocabularyByName('Direction dédiée');
          if($direction_vocabulary){
            // Recherche du département de l'utilisateur
            $direction_term = \Drupal::entityTypeManager()
                ->getStorage('taxonomy_term')
                ->loadByProperties(['name' => $user['department'], 'vid' => $direction_vocabulary->id()]);
            // Création du département s'il n'existe pas encore
            if(empty($direction_term)){
              $direction_term = \Drupal\taxonomy\Entity\Term::create([
                'name' => $user['department'],
                'vid' => $direction_vocabulary->id()
              ]);
              $direction_term->save();
              // Attribution du département à l'utilisateur drupal
              $newUser->field_departement = $direction_term->id();
            }else{
              $direction_term_id = array_keys($direction_term)[0];
              $newUser->field_departement = (int) $direction_term_id;
            }
          }

          // Niveaux hiérarchiques concernés
          $hierachie_vocabulary = $this->getVocabularyByName('Niveaux hiérarchiques concernés');
          if($hierachie_vocabulary){
            $hierachie_term = \Drupal::entityTypeManager()
                ->getStorage('taxonomy_term')
                ->loadByProperties(['name' => $user['fonction'], 'vid' => $hierachie_vocabulary->id()]);
            if(empty($hierachie_term)){
              $hierachie_term = \Drupal\taxonomy\Entity\Term::create([
                'name' => $user['fonction'],
                'vid' => $hierachie_vocabulary->id()
              ]);
              $hierachie_term->save();
              $newUser->field_fonction = $hierachie_term->id();
            }else{
              $hierachie_term_id = array_keys($hierachie_term)[0];
              $newUser->field_fonction = (int) $hierachie_term_id;
            }
          }

          $newUser->field_firstname = $user['nom'];
          $newUser->field_lastname = $user['prenom'];
          $newUser->entity_activity_mail_frequency = 'immediately';
          $newUser->roles = array(
            'authenticated' => 'Authenticated user'
          );
          $newUser->save();
          user_login_finalize($newUser);
          // $messenger->addMessage($user["email"] . ' create !');
        }
        curl_close ($ch);
      }else{
        $messenger->addMessage("Utilisateur inexistant !");
        return;
      }
    }

    // Redirect to home.
    $form_state->setRedirect('<front>');
  }


  function getVocabularyByName($vocabulary_name) {
    $vocabs = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple();
    // dump($vocabs);
    foreach ($vocabs as $vocab_object) {
      if ($vocab_object->get("name") == $vocabulary_name) {
        return $vocab_object;
      }
    }
    return NULL;
  }

}
