<?php
    @$civilite = $_POST["civilite"];
    @$nom = $_POST["nom"];
    @$prenoms = $_POST["prenoms"];
    @$email = $_POST["email"];
    @$mot_de_passe = $_POST['mot_de_passe'];
    @$repeat_mot_de_passe = $_POST['repeat_mot_de_passe'];

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Test</title>
    </head>
    <body>
        <form action="" method="post">
            <select name="civilite" id="civilite">
                <option value="mlle">Mademoiselle</option>
                <option value="mme">Madame</option>
                <option value="monsieur">Monsieur</option>
            </select>
            <input type="text" name="nom" id="nom">
            <input type="text" name="prenoms" id="prenoms">
            <input type="email" name="email" id="email">
            <input type="password" name="mot_de_passe" id="mot_de_passe">
            <input type="password" name="repeat_mot_de_passe" id="repeat_mot_de_passe">
        </form>
    </body>
</html>