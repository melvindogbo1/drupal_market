<?php
   require_once __DIR__ . '\fpdf\fpdf.php';
   require_once __DIR__ . '\fpdi\src\autoload.php';
    
    class FileDownload extends ControllerBase{
        /*public function addWatermark($x, $y, $watermarkText, $pdf){
            $pdf->Text($x, $y, $watermarkText);
        }*/
        public function fileGenerator($file){
            $current_user_id = \Drupal::currentUser()->id();
            $user_firstname = $current_user_id->field_firstname->value;
            $user_lastname = $current_user_id->field_lastname->value;
            $user_fullname = $user_lastname." ".$user_firstname;
            $new_file = str_replace(" ", "-", $file);
            rename($file, $new_file);
            $pdf = new \setasign\Fpdi\Fpdi();
            $fileInput = 'uploads/'.$_FILES['doc']['name'];
            //var_dump($fileInput);
            //var_dump($test_info);
            //$newFile = "new.pdf";
            shell_exec("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=new.pdf $new_file"); 
            try {
                $pages_count = $pdf->setSourceFile("new.pdf");
                for ($i = 1; $i <= $pages_count; $i++) {
                    //$pdf->AddPage('L');
                    $tplIdx = $pdf->importPage($i);
                    $size = $pdf->getTemplateSize($tplIdx);
                    //var_dump($size);
                    // Checking good resolutions for the file (Portrait or Landscape)
                    if ($size['width'] > $size['height']) {
                        $pdf->AddPage('L', array($size['width'], $size['height'] + 7));
                    } else {
                        $pdf->AddPage('P', array($size['width'], $size['height'] + 7));
                    }
                    $pdf->useTemplate($tplIdx, 0, 0);
                    $pdf->SetFont('Times', 'B', 8);
                    $pdf->SetTextColor(0, 0, 0);
                    $watermarkText = $user_fullname.' '.date('d/m/Y H:i');
                    $tx = $size['width'];
                    $ty = $size['height'];
                    //addWatermark($tx, $ty, $watermarkText, $pdf);
                    $pdf->Text($tx-70, $ty+3, $watermarkText);
                    $pdf->SetXY(0, 0);
                }  
                $pdf->Output();
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }        
    }